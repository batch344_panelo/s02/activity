import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {

        //Array

        int[] intArray = new int[5];

        intArray[0] = 2;
        intArray[1] = 3;
        intArray[2] = 5;
        intArray[3] = 7;
        intArray[4] = 11;

        System.out.println("The first prime number is: " + intArray[0]);
        System.out.println("The first prime number is: " + intArray[1]);
        System.out.println("The first prime number is: " + intArray[2]);
        System.out.println("The first prime number is: " + intArray[3]);
        System.out.println("The first prime number is: " + intArray[4]);

        //ArrayList

        ArrayList<String> friends = new ArrayList<String>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));

        System.out.println("My friends are: " + friends);

        //HashMap

        HashMap<String, Integer> inventoryList = new HashMap<String, Integer>();

        inventoryList.put("toothpaste", 15);
        inventoryList.put("toothbrush", 20);
        inventoryList.put("soap", 12);

        System.out.println("Our current inventory consists of: " + inventoryList);








    }
}